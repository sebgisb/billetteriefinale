<?php 

namespace Seb\RecaptchaBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RecaptchaException extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
    }
}