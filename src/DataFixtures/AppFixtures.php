<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\Event;
use App\Entity\Category;
use App\Entity\Artiste;
use App\Entity\Contact;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\Order;


class AppFixtures extends Fixture
{
    public function __construct(UserPasswordHasherInterface $passwordHasher)
     {
         $this->passwordHasher = $passwordHasher;
     }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        

        $faker = Faker\Factory::create('fr_FR');

        for($c = 0; $c < 12; $c++){
            $contact = new Contact();
                $contact->setLastname($faker->lastName())
                    ->setFirstname($faker->firstName())
                    ->setPhoneNumber($faker->isbn10())
                    ->setAdress($faker->streetAddress())
                    ->setCity($faker->city())
                    ->setCreatedAt(date_create(date("Y-m-d H:i:s",rand(1542111110 ,1570030952))));
            
                $this->addReference('contact-' . $c, $contact);
            
            $manager->persist($contact);
        }


        $userA = new User();
        $userA->setUsername('admin')
            ->setPassword($this->passwordHasher->hashPassword($userA,'admin'))
            ->setRoles(['ROLE_ADMIN'])
            ->setContact($this->getReference('contact-' . 1));
                         
        $manager->persist($userA);                 
        
        $user = new User();
        $user->setUsername('user')
            ->setPassword($this->passwordHasher->hashPassword($user,'user'))
            ->setRoles(['ROLE_USER'])
            ->setContact($this->getReference('contact-' . 0));
            
            

        $manager->persist($user);  

        for($u = 0; $u < 10; $u++){
            $user = new User();
            $user->setUsername($faker->username())
                ->setPassword($this->passwordHasher->hashPassword($user,'password'))
                // ->setContact($this->addReference('contact-' . $k, $contact))
                ->setRoles(['ROLE_USER'])
                ->setContact($this->getReference('contact-' . rand(2,11)));
            
                $this->addReference('user-'.$u , $user);
                
                $manager->persist($user);
        }

       for($a = 0; $a <200; $a++){
           $artiste = new Artiste();
           $artiste->setName($faker->firstName);

           $this->addReference('artiste-'. $a , $artiste);
           $manager->persist($artiste);
           
       }

      

        $test= 2;
        for($l = 0; $l < 30; $l++){
            $location = new Location();
            $location->setPlace($faker->name())
                ->setAdresse($faker->streetAddress())
                ->setCity($faker->city())
                ->setPlaces($faker->numberBetween(60, 2000));

                $this->addReference('location-' . $l, $location);

            $manager->persist($location);
        }

        // $categories = ['Sport','Spectacle','Concert','Festival'];

        $sports = ['Tennis', 'Football','Rugby', 'Basket', 'Handball', 'Athlétisme'];
        $sportsFiles = ['tennis.jpg', 'football.jpg','rugby.jpg', 'basket.jpg', 'handball.jpg', 'athletisme.jpg'];

        $concerts = ['Pop / Rock', 'Variété française', 'Variété international', 'Musique Classique', 'Musique Electro', 'Rap / Hip-Hop'];
        $concertsFiles = ['poprock.jpg', 'varietefr.jpg', 'varieteinter.jpg', 'classique.jpg', 'electro.jpg', 'rap.jpg'];

        $festivals = ['Festival Musique', 'Festival Humour', 'Festival Théâtre', 'Festival Classique et Opéra'];
        $festivalsFiles = ['festivalmusique.jpg', 'festivalhumour.jpg', 'festivaltheatre.jpg', 'festivalclassique.jpg'];

        $spectacles = ['Comédie musicale', 'Grand spectacle', 'Spectacle jeune publique','Spectacle musical','Danse','Cirque'];
        $spectaclesFiles = ['comediemusical.jpg', 'grandspectacle.jpg', 'spectaclejeune.jpg','festivalclassique.jpg','danse.jpg','cirque.jpg'];
        
        // foreach( $categories as $k => $cat){
        //     $category = new Category();
        //     $category->setLabel($cat)
        //         ->setFilename($faker->imageUrl(640,480));

        //     $manager->persist($category);
        // }


        foreach( $sports as $k => $sport){
            $category = new Category();
            $category->setLabel($sport)
                ->setParent('Sport')
                ->setFilename($faker->imageUrl(640,480));
                
                $manager->persist($category);

                $this->addReference('sport-' . $k, $category);
        }

        foreach( $concerts as $k => $concert){
            $category = new Category();
            $category->setLabel($concert)
                ->setParent('Concert')
                ->setFilename($faker->imageUrl(640,480));
                
                $manager->persist($category);

                $this->addReference('concert-' . $k, $category);
        }

        foreach( $festivals as $k => $festival){
            $category = new Category();
            $category->setLabel($festival)
                ->setParent('Festival')
                ->setFilename($faker->imageUrl(640,480));
                
                $manager->persist($category);

                $this->addReference('festival-' . $k, $category);
        }

        foreach( $spectacles as $k => $spectacle){
            $category = new Category();
            $category->setLabel($spectacle)
                ->setParent('Spectacle')
                ->setFilename($faker->imageUrl(640,480));
                
                $manager->persist($category);

                $this->addReference('spectacle-' . $k, $category);
        }

        
        for($s = 0; $s < 50; $s++){

            $int= rand(1572111110 ,1594030952);
            $highInt = $int + rand(7200,259200);

            $randKey = rand(0,5);

            $event = new Event();
            $event->setCategory($this->getReference('spectacle-' . $randKey))
                ->setCreatedAt(date_create(date("Y-m-d H:i:s",rand(1542111110 ,1570030952))))
                ->setStartDate(date_create(date("Y-m-d H:i:s",$int)))
                ->setEndDate(date_create(date("Y-m-d H:i:s", $highInt )))
                ->setDescription($faker->text(400))
                ->setTitle($faker->sentence(3, true))
                ->setArchive(false)
                ->setPutForward(false)
                ->setBeginningHour(date_create(date("Y-m-d H:i:s",$int)))
                ->setEndHour(date_create(date("Y-m-d H:i:s", $highInt )))
                ->setLocation($this->getReference('location-' . rand(0,29)))
                ->setFilename($spectaclesFiles[$randKey])
                ->setPrice(rand(40, 200));

            $manager->persist($event);
        }

        for($b = 0; $b < 50; $b++){

            
            $int= rand(1572111110 ,1594030952);
            $highInt = $int + rand(7200,259200);

            $randKey = rand(0,5);

            $event = new Event();
            $event->setCategory($this->getReference('sport-' . $randKey))
                ->setCreatedAt(date_create(date("Y-m-d H:i:s",rand(1542111110 ,1570030952))))
                ->setStartDate(date_create(date("Y-m-d H:i:s",$int)))
                ->setEndDate(date_create(date("Y-m-d H:i:s", $highInt )))
                ->setDescription($faker->text(400))
                ->setTitle($faker->sentence(3, true))
                ->setArchive(false)
                ->setPutForward(false)
                ->setBeginningHour(date_create(date("Y-m-d H:i:s",$int)))
                ->setEndHour(date_create(date("Y-m-d H:i:s", $highInt )))
                ->setLocation($this->getReference('location-' . rand(0,29)))
                ->setFilename($sportsFiles[$randKey])
                ->setPrice(rand(40, 200));

            $manager->persist($event);
        }

        for($c = 0; $c < 50; $c++){


            $int= rand(1572111110 ,1594030952);
            $highInt = $int + rand(7200,259200);

            $randKey = rand(0,5);

            $event = new Event();
            $event->setCategory($this->getReference('concert-' . $randKey))
                ->setCreatedAt(date_create(date("Y-m-d H:i:s",rand(1542111110 ,1570030952))))
                ->setStartDate(date_create(date("Y-m-d H:i:s",$int)))
                ->setEndDate(date_create(date("Y-m-d H:i:s", $highInt )))
                ->setDescription($faker->text(400))
                ->setTitle($faker->sentence(3, true))
                ->setArchive(false)
                ->setPutForward(false)
                ->setBeginningHour(date_create(date("Y-m-d H:i:s",$int)))
                ->setEndHour(date_create(date("Y-m-d H:i:s", $highInt )))
                ->setLocation($this->getReference('location-' . rand(0,29)))
                ->setFilename($concertsFiles[$randKey])
                ->setPrice(rand(40, 200));
                
            $this->addReference('event-'. $c, $event);

            $manager->persist($event);
        }

        for($d = 0; $d < 50; $d++){

            $int= rand(1572111110 ,1594030952);
            $highInt = $int + rand(7200,259200);

            $randKey =rand(0,3);

            $event = new Event();
            $event->setCategory($this->getReference('festival-' . $randKey))
                ->setCreatedAt(date_create(date("Y-m-d H:i:s",rand(1542111110 ,1570030952))))
                ->setStartDate(date_create(date("Y-m-d H:i:s",$int)))
                ->setEndDate(date_create(date("Y-m-d H:i:s", $highInt )))
                ->setDescription($faker->text(400))
                ->setTitle($faker->sentence(3, true))
                ->setArchive(false)
                ->setPutForward(false)
                ->setBeginningHour(date_create(date("Y-m-d H:i:s",$int)))
                ->setEndHour(date_create(date("Y-m-d H:i:s", $highInt )))
                ->setLocation($this->getReference('location-' . rand(0,29)))
                ->setFilename($festivalsFiles[$randKey])
                ->addArtiste($this->getReference('artiste-'. rand(0,199)))
                ->setPrice(rand(40, 200));

            $manager->persist($event);
        }
        for($o = 0; $o < 30; $o ++){

            $int= rand(1572111110 ,1594030952);
            $order = new Order();

            $order->setCreatedAt(date_create(date("Y-m-d H:i:s",$int))) 
                ->setTicketNumber(rand(1,9))
                ->setUser($this->getReference('user-'.rand(0,9)))
                ->addEvent($this->getReference('event-'.rand(0,49)));

            $manager->persist($order);
        }
        
       
        $manager->flush();
    }
}
