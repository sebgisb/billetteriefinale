<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Artiste;
use App\Entity\Category;
use DateTime;
use App\Entity\Contact;
use App\Entity\Location;
use App\Entity\User;
use App\Form\EventType;
use App\Form\ArtisteType;
use App\Form\CategoryType;
use App\Form\UserType;
use App\Form\ContactType;
use App\Form\LocationType;
use App\Repository\EventRepository;
use App\Repository\ArtisteRepository;
use App\Repository\CategoryRepository;
use App\Repository\ContactRepository;
use App\Repository\LocationRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class AdminController extends AbstractController
{
    public function __construct(PaginatorInterface $paginator,EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->paginator = $paginator;
       
    }

    /**
     * @Route("/Billetterie/admin", name="admin", methods ={"GET"})
     *
     * @return void
     */
    public function homepage(Request $request): Response{
    
        return $this->render("admin/admin.html.twig", [
            "title" => "Admin",
            
        ]);

    }

    /**
     * @Route("/Billetterie/admin/event", name="admin-event", methods ={"GET", "POST"})
     *
     * @return void
     */
    public function adminEvent(EventRepository $eventRepository,Request $request): Response{
    
        $date = new DateTime();
        $event = new Event();
        
        $formEvent = $this->createForm(EventType::class, $event);
        $formEvent->handleRequest($request);
        $event->setCreatedAt($date)
            ->setArchive(0);

        if($formEvent->isSubmitted()){
            $this->em->persist($formEvent->getData());
            $this->em->flush();
        }

        $events = $this->paginator->paginate($eventRepository->findAll(),
        $request->query->getInt('page',1),20);
        
        return $this->render("admin/eventAdmin.html.twig", [
            "title" => "Admin",
            "sousTitre" =>'gestion événements',
            "form" => $formEvent->createView(),
            "events" => $events
        ]);

    }

      /**
     * @Route("/Billetterie/admin/event/{id}", name="edit-event", methods ={"GET", "POST"})
     *
     * @return void
     */
    public function editEvent(int $id, EventRepository $eventRepository,Request $request): Response{
    
        $event = $eventRepository->findById($id);
        
        $formEvent = $this->createForm(EventType::class, $event[0]);

        $formEvent->handleRequest($request);

        if($formEvent->isSubmitted()){
            $this->em->persist($formEvent->getData());
            $this->em->flush();
        }

        return $this->render("admin/editAdmin.html.twig", [
            "title" => "Admin",
            "sousTitre" =>'Modification Event',
            'event' => $event,
            'form' => $formEvent->createView(),
            
        ]);

    }

      /**
     * @Route("/Billetterie/admin/artiste", name="admin-artiste", methods ={"GET", "POST"})
     *
     * @return void
     */
    public function adminArtiste(ArtisteRepository $artisteRepository,Request $request): Response{
    
        $artiste = new Artiste();
        $formArtiste = $this->createForm(ArtisteType::class, $artiste);

        $formArtiste->handleRequest($request);

        if($formArtiste->isSubmitted()){
            $this->em->persist($formArtiste->getData());
            $this->em->flush();
        }

        $artistes = $this->paginator->paginate($artisteRepository->findAll(),
        $request->query->getInt('page',1),20);

        return $this->render("admin/artisteAdmin.html.twig", [
            "title" => "Admin",
            "sousTitre" =>'gestion artiste',
            // "form" => $formArtiste->createView(),
            'artistes' => $artistes,
            'form' => $formArtiste->createView(),
            
        ]);

    }

      /**
     * @Route("/Billetterie/admin/location", name="admin-location", methods ={"GET", "POST"})
     *
     * @return void
     */
    public function adminLocation(LocationRepository $locationRepository,Request $request): Response{
    

        $location = new Location();
        $formLocation = $this->createForm(LocationType::class, $location);

        $formLocation->handleRequest($request);
        
        if($formLocation->isSubmitted()){
            $this->em->persist($formLocation->getData());
            $this->em->flush();
        }

        $locations = $this->paginator->paginate($locationRepository->findAll(),
        $request->query->getInt('page',1),20);

        return $this->render("admin/locationAdmin.html.twig", [
            "title" => "Admin",
            "sousTitre" =>'gestion salle',
            // "form" => $formLocation->createView()
            "locations" => $locations,
            'form' => $formLocation->createView(),
            
        ]);

    }

    /**
     * @Route("/Billetterie/admin/location/{id}", name="edit-location", methods ={"GET", "POST"})
     *
     * @return void
     */
    public function editLocation(int $id, LocationRepository $locationRepository,Request $request): Response{
    
        $location = $locationRepository->findById($id);
        
        $formLocation = $this->createForm(LocationType::class, $location[0]);

        $formLocation->handleRequest($request);

        if($formLocation->isSubmitted()){
            $this->em->persist($formLocation->getData());
            $this->em->flush();
        }

        return $this->render("admin/editAdmin.html.twig", [
            "title" => "Admin",
            "sousTitre" =>'Modification Salle',
            'location' => $location,
            'form' => $formLocation->createView(),
            
        ]);

    }

      /**
     * @Route("/Billetterie/admin/catégorie", name="admin-category", methods ={"GET", "POST"})
     *
     * @return void
     */
    public function adminCategory( CategoryRepository $categoryRepository,Request $request): Response{
    

        $category = new Category();
        $formCategory = $this->createForm(CategoryType::class, $category);

        $formCategory->handleRequest($request);

        if($formCategory->isSubmitted()){
            $this->em->persist($formCategory->getData());
            $this->em->flush();
        }

        $categories = $categoryRepository->findAll();

        return $this->render("admin/categoryAdmin.html.twig", [
            "title" => "Admin",
            "sousTitre" =>'gestion catégories',
            // "form" => $formEvent->createView()
            "categories" => $categories,
            'form' => $formCategory->createView(),
            
        ]);

    }

      /**
     * @Route("/Billetterie/admin/user", name="admin-user", methods ={"GET", "POST"})
     *
     * @return void
     */
    public function adminUser(UserRepository $userRepository,Request $request): Response{
    

        $user = new User();
        $formUser = $this->createForm(UserType::class, $user);

        $formUser->handleRequest($request);

        if($formUser->isSubmitted()){
            $this->em->persist($formUser->getData());
            $this->em->flush();
        }

        $users = $this->paginator->paginate($userRepository->findAll(),
        $request->query->getInt('page',1),20);


        return $this->render("admin/userAdmin.html.twig", [
            "title" => "Admin",
            "sousTitre" =>'gestion utilisateurs',
            // "form" => $formUser->createView()
            'users' => $users,
            'form' => $formUser->createView(),
            
        ]);

    }

    

      /**
     * @Route("/Billetterie/admin/vente", name="admin-sell", methods ={"GET", "POST"})
     *
     * @return void
     */
    public function adminSell(Request $request): Response{
    
        $event = new Event();
        $formEvent = $this->createForm(EventType::class, $event);

        if($formEvent->isSubmitted()){
            $this->em->persist($formEvent->getData());
            $this->em->flush();
        }

        return $this->render("admin/formAdmin.html.twig", [
            "title" => "Admin",
            "sousTitre" =>'gestion événements',
            // "form" => $formEvent->createView()
            
        ]);

    }
}

?>