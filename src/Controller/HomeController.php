<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    public function __construct(EventRepository $EventRepository, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->eventRepo = $EventRepository;
    }

    /**
     * @Route("/Billetterie", name="accueil", methods ={"GET"})
     *
     * @return void
     */
    public function homepage(Request $request): Response{
    
        $eventForwards = $this->eventRepo->findForward();
        
        return $this->render("front/home.html.twig", [
            "title" => "Billetterie",
            "events" => $eventForwards
        ]);

    }

}

?>