<?php

namespace App\Controller;

use App\Entity\Category;


use App\Data\SearchData;
use App\Form\SearchType;
use App\Repository\EventRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class EventController extends AbstractController
{
    
    

     /**
     * @Route("/Billetterie/événements/{cat}", name="events", methods ={"GET"})
     */
    public function events($cat,PaginatorInterface $paginator,CategoryRepository $categoryRepository, EventRepository $eventRepository, Request $request): Response
    {
        $search = new SearchData();
      
        $form =$this->createForm(SearchType::class, $search);
        
        if($request->get('search')){
            $filters = $request->get('search');
            $search->category =(!empty($filters['category'])) ? $categoryRepository->findMultipleById($filters['category']) : 0;
            $search->min = (!empty($filters['min'])) ? $filters['min'] :0;
            $search->max = (!empty($filters['max'])) ? $filters['max'] :100000;
            $evenements = $eventRepository->findSearch($search);
            
        }else{
            $evenements =  $paginator->paginate($eventRepository->findByCatParent($cat),
            $request->query->getInt('page',1),12);
        }

        
        if($request->get('ajax')){
            return new JsonResponse([
                'content' => $this->renderView('parts/_listEvents.html.twig', [
                'events' => $evenements,
                ])
            ]);
           
        }
        // dd($evenements);
        return $this->render('front/events.html.twig', [
            'title' => $cat,
            'events' => $evenements,
            'form' => $form->createView(),
            
           
        ]);
    }


     /**
     * @Route("/Billetterie/Categorie/Search", name="search", methods ={"GET"})
     */
    public function search(PaginatorInterface $paginator, EventRepository $eventRepository,CategoryRepository $categoryRepository, Request $request): Response
    {
        $form =$this->createForm(SearchType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() ) {
            $data = $form->getData();
            $evenements = $eventRepository->findSearch($data);   
            
        }

        return $this->render('front/events.html.twig', [
            'title' => 'Recherche',
            'events' => $evenements,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/Billetterie/SingleEvent/{id}", name="singleEvent", methods={"GET", "POST"})
     *
     * @return void
     */
    public function singleEvent(EventRepository $eventRepository, int $id)
    {
        $event = $eventRepository->findById($id);

        $c= $eventRepository->find($id)->getCategory();
        $eventSimilaire = $eventRepository->find4lastByCat($c);

        return $this->render('front/singleEvent.html.twig', [
            'title' => "Single event",
            'event' => $event,
            'eventSimilaire' => $eventSimilaire,
        ]);
    }
}
