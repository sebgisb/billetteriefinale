<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Order;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\EventRepository;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UserController extends AbstractController
{
    public function __construct(EventRepository $EventRepository, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->eventRepo = $EventRepository;
    }

    /**
     * @Route("/Billetterie/MonCompte", name="monCompte", methods ={"GET"})
     *
     * @return void
     */
    public function userAccount(OrderRepository $orderRepository, SessionInterface $session, Request $request): Response
    {
        $user = $this->getUser();
        $orders = $orderRepository->findByUserId($user);
        
        
        return $this->render("user/monCompte.html.twig", [
            "title" => "Mon Compte",
            'orders' => $orders,
            
        ]);

    }


    /**
     * @Route("/Billetterie/Panier", name="cart", methods ={"GET"})
     *
     * @return void
     */
    public function cart(SessionInterface $session, Request $request): Response
    {
        $cart = $session->get('cart', []);

        $session->set("cart", $cart);
        
        $dataCart = [];
        $totalAll = 0;
        
        foreach($cart as $id => $quantity){
            $article = $this->eventRepo->find($id);
            
            if($quantity > 0){   
                $dataCart[] = [
                    "article" => $article,
                    "quantity" => $quantity,
                    "total" => $article->getPrice() * $quantity
                ];
            }

            $totalAll += $article->getPrice() * $quantity;
            
        }
        
        return $this->render("user/cart.html.twig", [
            "title" => "Panier",
            "dataCart" => $dataCart,
            "total" => $totalAll
            
        ]);

    }

    /**
     * @Route("/Billetterie/MonCompte/Panier/{id}", name="cart_manage", methods={"GET"})
     *
     * @return void
     */
    public function manageCart(Event $event, int $id, SessionInterface $session, Request $request)
    {
        $cart = $session->get('cart', []);

        $id = $event->getId();

        // dd($cart[$id]);
        
        if(!empty($cart[$id]) && $_GET['number'] != 0){

            $cart[$id]+= $_GET['number'];
        }
        else{
            $cart[$id] = $_GET['number'];
        }
        $session->set("cart", $cart);
        
        return $this->redirectToRoute("cart");
    }

    /**
     * @Route("/Billetterie/Order", name="order")
     *
     * @return void
     */
    public function order(SessionInterface $session, EntityManagerInterface $em)
    {

        $cart = $session->get('cart', []);

        $session->set("cart", $cart);
        
        
        $dateNow = new DateTime();
        $user = $this->getUser();

        foreach($cart as $id => $quantity){
            $article = $this->eventRepo->find($id);

            if($quantity > 0){
                $order = new Order();
                $order->setUser($user)
                    ->addEvent($article)
                    ->setTicketNumber($quantity)
                    ->setCreatedAt($dateNow);
                $this->em->persist($order);
                $this->em->flush();
            }
           
        } 

        return $this->redirectToRoute("monCompte");
    }

}

?>