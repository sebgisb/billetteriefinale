<?php

namespace App\Data;

class SearchData
{
    /**
     * Undocumented variable
     *
     * @var integer
     */
    public $page= 1;

    public $q = '';

    public $category;

    public $max;

    public $min;

    
}