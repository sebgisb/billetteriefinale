<?php

namespace App\Form;

use App\Entity\Event;
use App\Entity\Category;
use App\Entity\Location;
use App\Entity\Artiste;
use App\Repository\ArtisteRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => "Titre :",
            ])
            // ->add('created_at')
            ->add('start_date',  DateType::class,[
                'required'=>true,
                'label' => "Date de début :",
                'widget' => 'single_text',
            ])
            ->add('end_date', DateType::class,[
                'required'=>true,
                'label' => "Date de fin :",
                'widget' => 'single_text',
            ])
            ->add('description', null ,[
                'required'=>true,
                'label' => "Description :"
            ])
            ->add('price', null ,[
                'required'=>true,
                'label' => "Prix :"
            ])
            // ->add('child_price', null ,[
            //     'required'=>false,
            //     'label' => "Prix enfant :"
            // ])
            ->add('beginning_hour', TimeType::class ,[
                'required'=>true,
                'label' => "Heure de début :",
                'widget' => 'single_text',
            ])
            ->add('end_hour', TimeType::class ,[
                'required'=>true,
                'label' => "Heure de fin :",
                'widget' => 'single_text',
            ])
            // ->add('archive')
            ->add('imageFile', FileType::class, [
                "required" => true,
                "label" => "Image :"
            ])
            ->add('put_forward', null ,[
                'required'=>false,
                'label' => "Mettre cet événements en avant ? :"
                
            ])
            ->add('category', EntityType::class, [
                'label' => 'Catégorie',
                'required' => true,
                'class' => Category::class,
                "choice_label" => 'label',
            ])
            ->add('location',EntityType::class, [
                'label' => 'Salle :',
                'required' => true,
                'class' => Location::class,
                "choice_label" => 'place',
            ])
            
            ->add('artiste', EntityType::class, [
                'label' => 'Intervenant-e-s :',
                'required' => true,
                'class' => Artiste::class,
                'choice_label'=> 'name',
                'multiple' => true,   
            ])
            // ->add('artiste', EntityType::class, [
            //     'label' => 'Intervenant-e-s :',
            //     'required' => true,
            //     'class' => Artiste::class,
            //     'query_builder' => function(ArtisteRepository $artisteRepository){ // organisation d'un tri alphabetique
            //         return $artisteRepository->findByNameOrder();
            //     },
            //     'choice_label'=> function($artiste){ // choix du champs d'affichage
            //         return $artiste->getName();
            //     },
                
            //     'multiple' => true
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
