<?php

namespace App\Form;

use App\Entity\Location;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('place', null, [
                'label' => "Nom de salle / lieu :",
            ])
            ->add('city', null, [
                'label' => "Ville :",
            ])
            ->add('adresse', null, [
                'label' => "Adresse :",
            ])
            ->add('places', null, [
                'label' => "Nombre de places :",
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Location::class,
        ]);
    }
}
