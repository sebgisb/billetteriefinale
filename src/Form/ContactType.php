<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', null, [
                'label' => "Ville :",
            ])
            ->add('adress', null, [
                'label' => "Adresse :",
            ])
            ->add('phone_number', null, [
                'label' => "Numéro de téléphone :",
            ])
            ->add('firstname', null, [
                'label' => "Nom :",
            ])
            ->add('lastname', null, [
                'label' => "Prénom :",
            ])
            // ->add('created_at')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
