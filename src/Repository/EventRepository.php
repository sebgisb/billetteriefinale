<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPaginationInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Event::class);
        $this->paginator = $paginator;
    }

    /**
     * Undocumented function
     *
     * @return PaginationInterface
     */
    public function findSearch(SearchData $search): PaginationInterface
    {

        $query = $this
            ->createQueryBuilder('e')
            ->where('e.archive = false')
            ->select('c','e')
            ->join('e.category','c');
            

            if(!empty($search->q)){
                $query = $query
                    ->andWhere('e.title LIKE :q')
                    ->setParameter('q', "%{$search->q}%");
            }

            if(!empty($search->min)) {
                $query = $query
                    ->andWhere('e.price >= :min')
                    ->setParameter('min', $search->min);
            }

            if(!empty($search->max)) {
                $query = $query
                    ->andWhere('e.price <= :max')
                    ->setParameter('max', $search->max);
            }

            if(!empty($search->category)){
                $query = $query
                    ->andWhere('c.id IN (:category)')
                    ->setParameter('category', $search->category);
            }


        $query = $query->getQuery();
        return $this->paginator->paginate(
            $query,
            $search->page,
            12
        );
    }

    public function findByCatParent(string $cat)
    {
        return $this->createQueryBuilder('e')
            ->join('e.category','c')
            ->where('c.parent= :cat')
            ->setParameter('cat', $cat)
            ->orderBy('e.start_date', 'ASC')    
            ->getQuery()
            ->getResult();
        ;
    }

    public function findForward()
    {
        return $this->createQueryBuilder('e')
           
            ->where('e.put_forward = 1')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
        ;
    }
}
