import './styles/events.scss';

function toggleFilter (){
    const filterMenu = document.getElementById('filter-menu');
    const filterButton = document.getElementById('filter-button');
    const navbar = document.getElementById('nav-menu');
    
    filterButton.addEventListener('click', ()=>{
        filterMenu.classList.toggle('show-filter');
        navbar.classList.remove('show-nav');
        console.log('click');
    })
}
toggleFilter();

