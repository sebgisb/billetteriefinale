/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';



// start the Stimulus application
import './bootstrap';

function toggleMenu (){
    const navbar = document.getElementById('nav-menu');
    const burger = document.getElementById('burger');
    const bar = document.getElementById('bar');
    const filterMenu = document.getElementById('filter-menu');
    burger.addEventListener('click', ()=>{
        navbar.classList.toggle('show-nav');
        bar.classList.toggle('bar-hidden');
        filterMenu.classList.remove('show-filter');
        console.log('click');
    })
}

toggleMenu();






