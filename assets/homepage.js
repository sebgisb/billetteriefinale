import './styles/homepage.scss';

function toggleSlide(){
    const slideLeft = document.getElementById('slide-left');
    const slideRight = document.getElementById('slide-right');
    const slider = document.getElementById('slider');
    const firstSlide = document.getElementById('slide-img');

    slideRight.addEventListener('click', ()=>{
        slider.scrollLeft += firstSlide.offsetWidth;
    });

    slideLeft.addEventListener('click', ()=>{
        slider.scrollLeft -= firstSlide.offsetWidth;
    });

    
}

toggleSlide();