import './styles/singleEvent.scss';

async function createMap(){
    const token = "pk.eyJ1Ijoic2ViMiIsImEiOiJja2gwOWF2M2cwemduMnFwY2dnZHd3bjk0In0.YVEQuoJBENiFWPlo-wDMlg";
    const adress = document.getElementById("adress").innerText;
    const url = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + adress + ".json?types=address&access_token="+ token;
    const coordinates = await fetch(url)
        .then(resultat =>resultat.json())
        .then(json =>json.features[0].center);
    
        mapboxgl.accessToken = token;
        var map = new mapboxgl.Map({
            container: 'map', // container ID
            style: 'mapbox://styles/mapbox/streets-v11', // style URL
            center: coordinates, // starting position [lng, lat]
            zoom: 10 // starting zoom
            });
            var marker = new mapboxgl.Marker()
            .setLngLat(coordinates)
            .addTo(map);
}
createMap();


let adultInput = document.getElementById("adult");

if(adultInput){
    // let childPrice = document.getElementById("childPrice").value;
    let adultInput = document.getElementById("adult");
    let adultPrice = document.getElementById("adultPrice").value;
    let total = document.getElementById("total");
    
    total.innerHTML = "0 €";
    // childInput.addEventListener('change', (event)=>{
    //     total.innerHTML = childPrice * parseInt(event.target.value) + parseInt(adultInput.value) * adultPrice + " €";
    // })
    
    adultInput.addEventListener('change', (event)=>{
        total.innerHTML = adultPrice*parseInt(event.target.value) + " €";
        console.log('test');
    })
}