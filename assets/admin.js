import './styles/admin.scss';


function toggleAdminForm(){

    const button = document.getElementById('create');
    const adminForm = document.getElementById('admin-form');

    button.addEventListener('click', ()=>{
        adminForm.classList.toggle('display-admin-form');
    })
}

toggleAdminForm();