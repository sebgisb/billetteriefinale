import './styles/login.scss';

const $triggerRegister = document.getElementById('trigger-register');
const $triggerLogin = document.getElementById('trigger-login');
const $loginForm = document.getElementById('login-form');
const $registerForm = document.getElementById('register-form');

function triggerLogin(){
  
    $triggerRegister.addEventListener('click', ()=>{
        $registerForm.classList.remove('hidden');
        $loginForm.classList.add('hidden');
    })
}

function triggerRegister(){
    $triggerLogin.addEventListener('click', ()=>{
        $loginForm.classList.remove('hidden');
        $registerForm.classList.add('hidden');
    })
}



triggerLogin();
triggerRegister();
